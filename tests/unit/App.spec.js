import { shallowMount, mount } from '@vue/test-utils'
import App from '@/App.vue'

// Minimalistic, placeholder tests. Needs to be alot more than this

describe('App', () => {
  it('has data', () => {
    expect(typeof App.data).toBe('function');
  });
});

describe('App.vue Test', () => {
  it('renders message when component is created', () => {
    // render the component
    const wrapper = shallowMount(App, {
      // propsData: {
      //   title: 'Vue Project'
      // }
    })

    // check the name of the component
    expect(wrapper.name()).toMatch('App')

  })
})

import { shallowMount, mount } from '@vue/test-utils'
import Attribute from '@/components/form/Attribute.vue'

// Minimalistic, placeholder tests. Needs to be alot more than this

describe('Attribute test', () => {
  let wrapper = null;

  // SETUP - run before to each unit test
  beforeEach(() => {
    // render the component
    wrapper = shallowMount(Attribute, {
      propsData: {
        kv: {idx: 1, key: 'somekey', value: 'Some Value'}
      }
    })
  })

  // TEARDOWN - run after to each unit test
  afterEach(() => {
    wrapper.destroy();
  })

  // Unit tests
  it('Check attribute name', () => {
    // Check the name of the component
    expect(wrapper.name()).toMatch('Attribute');
  })

  it('Check Props data', () => {
    // Check Props
    expect(wrapper.vm.kv.value).toMatch('Some Value');
    expect(wrapper.vm.kv.key).toMatch('somekey');
  })

  it('Check DOM Output', () => {
    // Check Dom
    expect(wrapper.findAll('span').length).toEqual(2)
    expect(wrapper.findAll('span').at(0).text()).toMatch('somekey')
    expect(wrapper.findAll('span').at(1).text()).toMatch('Some Value')
  })

})
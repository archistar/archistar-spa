import geoJson from '../../assets/data/testBlob.json';

// initial state
const state = () => ({
  geoJson,
  currentAttribute: {},
  marker: null
})

// getters
const getters = {}

// actions
const actions = {}

// mutations
const mutations = {
  setAttribute (state, attribute) {
    state.currentAttribute = attribute
  },
  setMarker (state, marker) {
    state.marker = marker
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

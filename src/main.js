import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import store from './store';

// Set Fontawesome globally
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTimesCircle, faStar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
library.add(faTimesCircle, faStar)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// Apply BootstrapVue and import Bootstrap css
Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'vue-slider-component/theme/default.css';
import './assets/css/app.css';

// Get root app
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');

import Mapbox from "mapbox-gl";

export const zoomToBounds = (store, coords) => {
  const map = store.mapContext;
  const features = store.state.map.geoJson.features;

  if (coords.length === 0) {
    features.forEach(feature => {
      coords.push(feature.geometry.coordinates);
    })
  }

  var bounds = coords.reduce(function(bounds, coord) {
    return bounds.extend(coord);
  }, new Mapbox.LngLatBounds(coords[0], coords[0]));

  map.fitBounds(bounds, {
    padding: 100
  });
}

export const applyFilter = (store, controls) => {
  // Pickup map and original features from store
  const map = store.mapContext;
  const features = store.state.map.geoJson.features;

  let data = {type: "FeatureCollection", features:[]};
  let coords = [];
  const optionControls = controls.filter(control => (control.type === 'range' || control.selected.length > 0));

  // Build feature selection
  data.features = features.filter(function(feature) {
    const project = feature.properties.project;
    let state = true;
    optionControls.forEach(control => {
      if (control.type === 'lookup') {
        const attributeValue = project[control.id];
        if (control.selected.indexOf(attributeValue) === -1) {
          state = false;
        }
      } 
      if (control.type === 'range') {
        const value = parseInt(project.Value)
        if ( value < control.value[0] || value > control.value[1]) {
          state = false;
        }
      }
    })
    return state;
  })

  coords = data.features.map(feature => {
    return feature.geometry.coordinates;
  })

  // Set filtered data directly on the map
  if (data.features.length > 0) {
    map.getSource('data').setData(data);
  } else {
    map.getSource('data').setData(store.state.map.geoJson);
  }

  // Zoom to bounds
  zoomToBounds(store, coords);
}

#!/bin/bash
IMAGE=matzpersson/archistar-spa:latest
NAME=archistar-spa
PORT=8080

docker build --no-cache -t $IMAGE .
docker run  -p $PORT:$PORT --rm -it --name $NAME $IMAGE
